import {CHANGE_THEME, DECREMENT, DISABLE_BUTTONS, ENABLE_BUTTONS, INCREMENT, SET_ACTIVE} from "./types";

export function increment() {
  return {
    type: INCREMENT
  }
}

export  function decrement() {
  return {
    type: DECREMENT
  }
}

export  function changeTheme(newTheme) {
  return {
    type:  CHANGE_THEME,
    payload: newTheme,
  }
}

export const enableButtons = () => ({
  type: ENABLE_BUTTONS,
})

export const disableButtons = () => ({
  type: DISABLE_BUTTONS,
})

export function asyncIncrement() {
  return function (dispatch) {
    dispatch( disableButtons() )
    setTimeout( () => {
      dispatch( increment() )
      dispatch( enableButtons() )
    }, 2000)
  }
}